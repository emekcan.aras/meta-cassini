# SPDX-FileCopyrightText: <text>Copyright 2024 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT

inherit features_check
REQUIRED_DISTRO_FEATURES = "cassini-test"

RDEPENDS:${PN}-ptest += "\
    k3s-integration-tests-ptest \
"
