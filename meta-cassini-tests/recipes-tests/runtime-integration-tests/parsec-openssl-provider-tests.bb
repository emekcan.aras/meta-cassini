# SPDX-FileCopyrightText: Copyright (c) 2024, Linaro Limited.
#
# SPDX-License-Identifier: MIT

SUMMARY = "Parsec OpenSSL Provider tests."
DESCRIPTION = "Run E2E test for Parsec OpenSSL Provider within a container. \
               Tests may be run standalone via \
               run-parsec-openssl-provider-tests, or via the ptest \
               framework using ptest-runner."
HOMEPAGE = "https://cassini.readthedocs.io/en/latest/"

LICENSE = "MIT"
# License file is in "layers/poky/meta/files/common-licenses".
# nooelint: oelint.var.licenseremotefile
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

TEST_FILES = "file://parsec-openssl-provider-tests.bats \
              file://parsec-openssl-provider-funcs.sh \
              file://health_check.sh "

inherit runtime-integration-tests
require runtime-integration-tests.inc
