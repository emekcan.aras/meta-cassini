#!/usr/bin/env bash
#
# SPDX-FileCopyrightText: Copyright (c) 2024, Linaro Limited.
#
# SPDX-License-Identifier: MIT

health_check() {

    echo "Listing providers..."
    provider_status=$(openssl list -providers | grep -c "Parsec OpenSSL Provider")
    if [ "${provider_status}" ]; then
        echo "Parsec Openssl Provider initialized"
        return 0
    else
        return 1
    fi

}

health_check
