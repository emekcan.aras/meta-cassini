# SPDX-FileCopyrightText: Copyright (c) 2023-2024, Linaro Limited.
#
# SPDX-FileCopyrightText: <text>Copyright 2024 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT

---
header:
  version: 14
  includes:
    - kas/include/xilinx-machines.yml

local_conf_header:
  meta-ledge-secure-cleanup: |

    # The meta-ledge-secure layer is not a distro layer and
    # should not be setting DISTRO_FEATURES, so we remove
    # the added ones here to rely only on the cassini distro
    # definition

    # Disable tpm2
    DISTRO_FEATURES:remove = "tpm2"

    # Disable udisks2 (for clevis)
    DISTRO_FEATURES:remove = "polkit"

    # Disable selinux
    DISTRO_FEATURES:remove = "acl xattr pam selinux"

    # Ignore parts of meta-ledge-secure as we only want the device specific
    # kernel patches and the grub patches
    BBMASK += "meta-ledge-secure/dynamic-layers/"
    BBMASK += "meta-ledge-secure/recipes-core/"
    BBMASK += "meta-ledge-secure/recipes-ledge/"
    BBMASK += "meta-ledge-secure/recipes-samples/"
    BBMASK += "meta-ledge-secure/recipes-security/"
    BBMASK += "meta-ledge-secure/recipes-support/"
    BBMASK += "meta-ledge-secure/recipes-test/"

  meta-xilinx-cleanup: |

    # Headless build only, so mask out Xilinx's graphics recipes
    BBMASK += "meta-xilinx/meta-xilinx-core/recipes-graphics/"

  meta-virtualization-cleanup: |

    # Not using the Xilinx U-Boot, so mask out the dynamic append
    BBMASK += "meta-virtualization/dynamic-layers/xilinx/"

  zynqmp-kria-starter-psa: |

    UEFI_CERT_FILE = "\
    ${TOPDIR}/../layers/meta-ts/meta-trustedsubstrate\
    /uefi-certificates/uefi_certs.tgz\
    "

    UEFI_CAPSULE_CERT_FILE = "capsule_certs.tgz"

    # meta-ts should set this softly. But as it doesn't, we have to force it
    PREFERRED_PROVIDER_virtual/kernel:zynqmp-kria-starter-psa = "linux-yocto"
    COMPATIBLE_MACHINE:pn-linux-yocto = "zynqmp-kria-starter-psa"

    # Add grub-efi
    EFI_PROVIDER ?= "grub-efi"
    MACHINE_FEATURES += "efi"

    KERNEL_IMAGETYPE = "Image"
    IMAGE_FSTYPES += "wic wic.gz wic.bmap"

    # The extra rootfs space is ignored when the wic image
    # is generated using the wks file from meta-ledge-secure.
    # Therefore, it's added here in the rootfs partition arguments.
    WKS_ROOTFS_PART_EXTRA_ARGS = "--extra-space ${CASSINI_ROOTFS_EXTRA_SPACE}K"
    GRUB_CFG_FILE = "\
    ${CASSINI_DISTRO_LAYERDIR}/dynamic-layers/meta-ledge-secure/wic/kv260-grub.cfg"
    WKS_FILE = "ledge-secure.wks.in"

    # This kernel fragment adds configurations for selinux
    # including CONFIG_SECURITY_DMESG_RESTRICT which prevents
    # dmesg from running by users other than root, and
    # hence prevents k3s tests from running correctly.
    KERNEL_FEATURES:remove = "fragment-1-selinux.cfg"

machine: zynqmp-kria-starter-psa
