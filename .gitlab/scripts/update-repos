#! /usr/bin/env python3
# SPDX-FileCopyrightText: <text>Copyright 2022,2024 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT

# Update clones of the repositories we need in KAS_REPO_REF_DIR to speed up
# later fetches

import sys
import os
import subprocess
import pathlib


def repo_shortname(url):
    # Taken from Kas (Repo.__getattr__) to ensure the logic is right
    from urllib.parse import urlparse
    url = urlparse(url)
    return ('{url.netloc}{url.path}'
            .format(url=url)
            .replace('@', '.')
            .replace(':', '.')
            .replace('/', '.')
            .replace('*', '.'))


repositories = (
    "https://git.yoctoproject.org/git/meta-arm",
    "https://github.com/Wind-River/meta-secure-core.git",
    "https://gitlab.arm.com/cassini/meta-cassini-bsp.git",
    "https://git.openembedded.org/meta-openembedded",
    "https://git.yoctoproject.org/git/meta-security",
    "https://github.com/kraj/meta-clang",
    "https://git.yoctoproject.org/git/meta-virtualization",
    "https://git.yoctoproject.org/git/poky",
    "https://gitlab.com/Linaro/trustedsubstrate/meta-ts.git",
    "https://github.com/Xilinx/meta-xilinx",
    "https://gitlab.com/Linaro/trustedsubstrate/meta-ledge-secure.git",
    "https://github.com/aws/meta-aws"
)

if __name__ == "__main__":
    if "KAS_REPO_REF_DIR" not in os.environ:
        print("KAS_REPO_REF_DIR needs to be set")
        sys.exit(1)

    base_repodir = pathlib.Path(os.environ["KAS_REPO_REF_DIR"])

    for repo in repositories:
        repodir = base_repodir / repo_shortname(repo)
        if repodir.exists():
            print("Updating %s..." % repo)
            subprocess.run(["git", "-C", repodir, "fetch"], check=True)
        else:
            print("Cloning %s..." % repo)
            subprocess.run(["git", "clone", "--bare", repo, repodir],
                           check=True)
