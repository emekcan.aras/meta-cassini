#!/usr/bin/env python3
# SPDX-FileCopyrightText: <text>Copyright 2022-2023 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT

import argparse
import json
import re
import sys
import yaml


def main():
    parser = argparse.ArgumentParser(
        description='Adds FVP config options to lava test files')

    parser.add_argument("fvpconf", help="Yocto build generated fvpconf file")
    parser.add_argument("lava_file", help="Input lava definition file")
    parser.add_argument("lava_out", default="lava_definition.yml",
                        help="Output lava definition file")

    args = parser.parse_args()

    with open(args.fvpconf, "r") as fvpconf_file:
        fvpconf = json.load(fvpconf_file)

        PatchLavaFile(fvpconf, args.lava_file, args.lava_out)


def PatchLavaFile(fvpconf, lava_file, lava_out):
    with open(lava_file) as lava_file:
        lava_data = yaml.safe_load(lava_file)

        # Update the fvpconf with parts of lava script that need retaining
        for action in lava_data['actions']:
            if "boot" in action and action['boot']['method'] == "fvp":
                fvpconf = ParseArgs(fvpconf, action['boot']['arguments'])

        # Turn off diagnostic output as it hangs the FVP after a while
        for arg, data in fvpconf['parameters'].items():
            if arg.endswith("diagnostics"):
                fvpconf['parameters'][arg] = "0"

        newArguments = []
        for arg, data in fvpconf['parameters'].items():
            newArguments.append("-C " + arg + "=\"" + data + "\"")

        for data in fvpconf['data']:
            newArguments.append("--data " + data)

        # Now update the lava file data with the fvpconf
        for i in range(len(lava_data['actions'])):
            if ("boot" in lava_data['actions'][i] and
                    lava_data['actions'][i]['boot']['method'] == "fvp"):
                lava_data['actions'][i]['boot']['arguments'] = newArguments

        # And finally output the new file
        with open(lava_out, 'w') as lava_out:
            yaml.dump(lava_data, lava_out,
                      default_flow_style=False)


def ParseArgs(fvpconf, lava_args):
    c_args = {}
    data = {}

    find_data = re.compile(r'^(-(C|-data))\s+(.+)?=(.+)$')
    find_replacement = re.compile(r'^.*{[0-9A-Z_]+}.*$')
    for arg in lava_args:
        match = find_data.match(arg)

        arg_type = match.group(1)
        arg_name = match.group(3)
        arg_data = match.group(4)

        if find_replacement.match(arg_data):
            if arg_type == "-C":
                c_args[arg_name] = arg_data
            elif arg_type == "--data":
                data[arg_name + "="] = arg_data
            else:
                print("Unknown arg type found: " + arg_type)

    # parameters is a dictionary
    fvpconf['parameters'].update(c_args)
    # data is a list of strings
    for i in range(len(fvpconf['data'])):
        if fvpconf['data'][i].startswith(tuple(data.keys())):
            element_name = fvpconf['data'][i].split('=')[0] + "="
            # Need to sort out the load address if one exists
            # as we want the version from the fvpconf file
            # not the original in the lava file
            load_address = fvpconf['data'][i].split('@')[1]
            if load_address != "":
                load_address = "@" + load_address
            fvpconf['data'][i] = element_name + \
                data[element_name].split('@')[0] + load_address

    return fvpconf


if __name__ == '__main__':
    main()
