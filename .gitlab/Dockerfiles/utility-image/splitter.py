#!/usr/bin/env python3
# SPDX-FileCopyrightText: <text>Copyright 2023 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT
#
# SCT sequence file splitter

import argparse
import curses
import logging
import numpy
import operator
import os
import sys


# Colors
normal = ''
red = ''
yellow = ''
green = ''

if os.isatty(sys.stdout.fileno()):
    try:
        curses.setupterm()
        setafb = curses.tigetstr('setaf') or bytes()
        normal = curses.tigetstr('sgr0').decode()
        red = curses.tparm(setafb, curses.COLOR_RED).decode()
        yellow = curses.tparm(setafb, curses.COLOR_YELLOW).decode()
        green = curses.tparm(setafb, curses.COLOR_GREEN).decode()
    except Exception:
        pass


# Parse Sequence file, used to tell which tests should run.
def seq_parser(file):
    temp_dict = list()
    lines = file.readlines()
    magic = 7
    # a test in a seq file is 7 lines, if not mod7, something wrong..
    if len(lines) % magic != 0:
        logging.error(f"{red}sequence file cut short{normal}, should be mod7")
        sys.exit(1)
    # the utf-16 char makes this looping a bit harder, so we use x+(i) where i
    # is next 0-6th
    # loop ever "7 lines"
    for x in range(0, len(lines), magic):
        # (x+0)[Test Case]
        # (x+1)Revision=0x10000
        # (x+2)Guid=XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX
        # (x+3)Name=InstallAcpiTableFunction
        # (x+4)Order=0xFFFFFFFF
        # (x+5)Iterations=0xFFFFFFFF
        # (x+6)(utf-16 char)
        # currently only add tests that are supposed to run, should add all?
        # 0xFFFFFFFF in "Iterations" means the test is NOT supposed to run
        if "0xFFFFFFFF" not in lines[x + 5]:
            seq_dict = {
                # from after "Name=" to end (5char long)
                "name": lines[x + 3].split('=')[1],
                # from after"Guid=" to the end, (5char long)
                "guid": lines[x + 2].split('=')[1],
                # from after "Iterations=" (11char long)
                "Iteration": lines[x + 5].split('=')[1],
                # from after "Revision=" (9char long)
                "rev": lines[x + 1].split('=')[1],
                # from after "Order=" (6char long)
                "Order": lines[x + 4].split('=')[1]
            }
            # put in a dict based on guid
            temp_dict.append(seq_dict)

    logging.info(f"Found {int(len(lines) / magic)} total test cases")
    logging.info(f"Found {len(temp_dict)} active test cases")
    return temp_dict


def write_output(split, splits, tests):
    outfile = f"{args.out_file_prefix}_{split}.seq"

    logging.debug(f"Split {split} of {splits}")

    output_db = numpy.array_split(tests, splits)[split - 1]
    logging.debug(f"{len(output_db)} tests in split")

    with open(outfile, "w", encoding="utf-16", newline='\r\n') as f:
        for i, x in enumerate(output_db):
            if i > 0:
                f.write("\n[Test Case]\n")
            else:
                f.write("[Test Case]\n")
            f.write("Revision=" + x["rev"])
            f.write("Guid=" + x["guid"])
            f.write("Name=" + x["name"])
            f.write("Order=" + x["Order"])
            f.write("Iterations=" + x["Iteration"])
            f.write("\0")


if __name__ == '__main__':
    me = os.path.realpath(__file__)
    here = os.path.dirname(me)
    active_tests = 0
    splits = 1

    parser = argparse.ArgumentParser(
        description='Generate a Sequence files.'
                    ' This program takes an SCT sequence files, and'
                    ' generates a number of smaller sequence files'
                    ' from it to allow parallel running of the tests.',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument(
        '--debug', action='store_true', help='Turn on debug messages')
    parser.add_argument('seq_file', nargs='?', help='Input .seq filename')
    parser.add_argument('out_file_prefix', default='output',
                        nargs='?', help='Output .seq filename prefix')
    parser.add_argument('--splits', default=2, type=int,
                        nargs='?', help='Number of sequence files requested')

    args = parser.parse_args()

    logging.basicConfig(
        format='%(levelname)s %(funcName)s: %(message)s',
        level=logging.DEBUG if args.debug else logging.INFO)

    ln = logging.getLevelName(logging.WARNING)
    logging.addLevelName(logging.WARNING, f"{yellow}{ln}{normal}")
    ln = logging.getLevelName(logging.ERROR)
    logging.addLevelName(logging.ERROR, f"{red}{ln}{normal}")

    if args.debug:
        logging.debug(f"DEBUG enabled")

    # We must have a seq file
    if args.seq_file is None:
        logging.error("No input .seq!")
        sys.exit(1)

    # files are encoded in utf-16
    with open(args.seq_file, "r", encoding="utf-16-le") as f:
        db1 = seq_parser(f)
        db1 = sorted(db1, key=operator.itemgetter('Order'))

    splits = args.splits

    logging.debug(f"Splits {splits}")
    for split in range(splits):
        logging.info(f"Split {split + 1} of Splits {splits}")
        write_output(split + 1, splits, db1)
