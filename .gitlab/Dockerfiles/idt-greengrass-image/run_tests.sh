#!/bin/bash
#
# SPDX-FileCopyrightText: Copyright (c) 2024, Linaro Limited.
#
# SPDX-License-Identifier: MIT
#
# The AWS CLI should be setup with credentials and configured to run this
# script with the following parameters:
# - AWS_ACCESS_KEY_ID
# - AWS_SECRET_ACCESS_KEY
# - AWS_DEFAULT_REGION
#
# This script switches to IDT role and runs the tests.
#
# Required Parameters:
# - IDT_ROLE
# - IDT_ROLE_SESSION_DURATION
# - TEST_TIMEOUT_MULTIPLIER
# - TEST_SESSION_NAME

set -e

# ---------------------------------------------
# Help Function
# ---------------------------------------------
Help()
{
  error_flag=1
  if [[ -z "$IDT_ROLE" ]]; then
    echo "IDT_ROLE must be set in environment" 1>&2
  elif [[ -z "$IDT_ROLE_SESSION_DURATION" ]]; then
    echo "IDT_ROLE_SESSION_DURATION must be set in environment" 1>&2
  elif [[ -z "$TEST_TIMEOUT_MULTIPLIER" ]]; then
    echo "TEST_TIMEOUT_MULTIPLIER must be set in environment" 1>&2
  elif [[ -z "$TEST_SESSION_NAME" ]]; then
    echo "TEST_SESSION_NAME must be set in environment" 1>&2
  else
    error_flag=0
  fi
  return $error_flag
}

# ---------------------------------------------
# Main Script
# ---------------------------------------------
Help

AWS_ID=$(aws sts get-caller-identity --query "Account" --output text)

read -r access_key secret_key session_token <<< "$( aws sts assume-role \
                                                        --role-arn "arn:aws:iam::${AWS_ID}:role/${IDT_ROLE}" \
                                                        --role-session-name "${TEST_SESSION_NAME}" \
                                                        --query "Credentials.[AccessKeyId,SecretAccessKey,SessionToken]" \
                                                        --output text \
                                                        --duration-seconds "${IDT_ROLE_SESSION_DURATION}" )" 2>/dev/null

set +x

if [[ -z "$access_key" ]] || [[ -z "$secret_key" ]] || [[ -z "$session_token" ]]; then
    echo "Couldn't switch to IDT role"
else
    echo "Using IDT Role: ${IDT_ROLE}"

# AWS CLI has issues when we create a new profile to run with a session token,
# therefore switch to using the environment variables authentication method
# temporarily

    export AWS_ACCESS_KEY_ID="$access_key"
    export AWS_SECRET_ACCESS_KEY="$secret_key"
    export AWS_SESSION_TOKEN="$session_token"
    jq -M '.auth.method="environment"' \
        ${TESTER_CFG_PATH}/config.json | sponge ${TESTER_CFG_PATH}/config.json

fi

set -x

echo "Running tests with timeout multiplier=${TEST_TIMEOUT_MULTIPLIER}"
/tester/devicetester_greengrass_v2_linux/bin/devicetester_linux_x86-64 run-suite \
                                                                       --update-idt n \
                                                                       --userdata userdata.json \
                                                                       --timeout-multiplier "${TEST_TIMEOUT_MULTIPLIER}"
