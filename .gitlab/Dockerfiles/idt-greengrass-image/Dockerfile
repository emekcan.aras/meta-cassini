# SPDX-FileCopyrightText: Copyright (c) 2024, Linaro Limited.
#
# SPDX-License-Identifier: MIT
ARG DOCKERHUB_MIRROR=

FROM ${DOCKERHUB_MIRROR}debian:bookworm

ARG local_user=cassini-ci
ARG user_id=900
ARG group_id=900

ARG AWS_ACCESS_KEY_ID=
ARG AWS_SECRET_ACCESS_KEY=
ARG AWS_DEFAULT_REGION=

RUN apt-get update \
  && apt-get install -y locales \
  && localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8
ENV LANG=en_US.utf8

RUN apt-get install --no-install-recommends -y \
    awscli \
    bc \
    bridge-utils \
    ca-certificates \
    curl \
    dnsmasq \
    gnupg2 \
    gpg \
    iproute2  \
    jq \
    less \
    moreutils \
    openssh-client \
    python3 \
    python3-setuptools \
    python3-pip \
    software-properties-common \
    unzip \
    wget \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN wget -O - 'https://apt.corretto.aws/corretto.key' | gpg --dearmor -o /usr/share/keyrings/corretto-keyring.gpg \
  && echo "deb [signed-by=/usr/share/keyrings/corretto-keyring.gpg] https://apt.corretto.aws stable main" | tee /etc/apt/sources.list.d/corretto.list \
  && apt-get update \
  && apt-get install -y java-11-amazon-corretto-jdk \
  && java -version

RUN cd /usr/local/bin \
  && curl -fL https://getcli.jfrog.io/v2 | sh \
  && chmod a+x /usr/local/bin/*

ENV GG_NUCLEUS="2.12.0"

RUN groupadd -g $group_id $local_user \
  && useradd --no-log-init -m -d /tester -g $local_user -u $user_id $local_user \
  && chown -R $local_user:$local_user /tester \
  && cd /tester \
  && curl --version \
  && url=$(curl --request GET "https://download.devicetester.iotdevicesecosystem.amazonaws.com/latestidt?HostOs=linux&ProductVersion=${GG_NUCLEUS}&TestSuiteType=GGV2" \
        --user ${AWS_ACCESS_KEY_ID}:${AWS_SECRET_ACCESS_KEY} \
        --aws-sigv4 "aws:amz:us-west-2:iot-device-tester" \
        | jq -r '.LatestBk["DownloadURL"]') \
  && echo url=$url \
  && curl $url --output devicetester.zip \
  && unzip devicetester.zip

RUN mkdir -p /tester/scripts

# This script is used to create keys and certificates from AWS side
COPY iot_device_provision.sh /tester/scripts
RUN chmod 755 /tester/scripts/iot_device_provision.sh

# This script is used to generate device.json before running the tests
COPY generate_device_config.sh /tester/scripts
RUN chmod 755 /tester/scripts/generate_device_config.sh

# This script is used to switch to IDT role and run tests
COPY run_tests.sh /tester/scripts
RUN chmod 755 /tester/scripts/run_tests.sh

# This script is used to clean used thing and group
COPY aws_cleanup.sh /tester/scripts
RUN chmod 755 /tester/scripts/aws_cleanup.sh

# Define fixed parameters
ENV GG_HOME="/greengrass/v2"

ENV TESTER_CFG_PATH="/tester/devicetester_greengrass_v2_linux/configs"

# Config IoT Device tester
RUN jq -M '( .[0].features[] | select(.name == "arch") ).value |= "aarch64" \
          | ( .[0].features[] | select(.name == "docker") ).value |= "yes" \
          | ( .[0].features[] | select(.name == "hsi") ).value |= "no" \
          | ( .[0].devices[] | select(.id == "<device-id>") | .operatingSystem) |= "Linux" \
          | ( .[0].devices[] | select(.id == "<device-id>") | .connectivity.auth.method) |= "password" \
          | ( .[0].devices[] | select(.id == "<device-id>") | .connectivity.auth.credentials.user) |= "cassini" \
          | ( .[0].devices[] | select(.id == "<device-id>") | .connectivity.auth.credentials.password) |= "cassini123" \
          | del(.[0].devices[0].connectivity.auth.credentials.privKeyPath) \
          | del(.[0].devices[0].connectivity.publicKeyPath)' \
          ${TESTER_CFG_PATH}/device.json | sponge ${TESTER_CFG_PATH}/device.json && cat ${TESTER_CFG_PATH}/device.json

RUN jq --arg region "${AWS_DEFAULT_REGION}" -M '.awsRegion=$region | .auth.method="file" | .auth.credentials.profile="default"' \
        ${TESTER_CFG_PATH}/config.json | sponge ${TESTER_CFG_PATH}/config.json && cat ${TESTER_CFG_PATH}/config.json

RUN jq --arg te_role "${IOT_TE_ROLE}" --arg install_dir "${GG_HOME}" \
       -M '. + {TempResourcesDirOnDevice:"/tmp/idt", \
                InstallationDirRootOnDevice:$install_dir, \
                PreInstalled:"yes", \
                GreengrassV2TokenExchangeRole:$te_role}' \
        ${TESTER_CFG_PATH}/userdata.json | sponge ${TESTER_CFG_PATH}/userdata.json

RUN jq 'del(.hsm)' ${TESTER_CFG_PATH}/userdata.json | sponge ${TESTER_CFG_PATH}/userdata.json

RUN jq 'del(.GreengrassNucleusZip)' ${TESTER_CFG_PATH}/userdata.json | sponge ${TESTER_CFG_PATH}/userdata.json

RUN cat ${TESTER_CFG_PATH}/userdata.json
