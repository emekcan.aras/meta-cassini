# SPDX-FileCopyrightText: Copyright (c) 2024, Linaro Limited.
#
# SPDX-License-Identifier: MIT
---

Build-GreenGrass-Parsec-Provider:
  extends: .resource-request
  stage: Build
  tags:
    - x86_64
  variables:
    DOCKER_IMAGE_NAME: amazoncorretto-image
  image: $CI_REGISTRY_IMAGE/$DOCKER_IMAGE_NAME:$DOCKER_IMAGE_TAG
  needs:
    - job: Build-Amazon-Docker-Image
      optional: true
  before_script:
    # Configure SSH for git
    - mkdir -p ~/.ssh
    - eval "$(ssh-agent -s)"
    - if [ -n "${SSH_PRIVATE_GITLAB+x}" ]; then
        echo "${SSH_PRIVATE_GITLAB}" | tr -d '\r' | ssh-add - > /dev/null;
      fi
    - ssh-keyscan -t rsa ${CI_SERVER_HOST}  >> ~/.ssh/known_hosts
    # Ensure git is usable without prompts
    - git config --global url.ssh://git@${CI_SERVER_HOST}.insteadOf
        https://${CI_SERVER_HOST}
    - git config --global user.email "you@example.com"
    - git config --global user.name "Your Name"
    - git config --global --add safe.directory ${CI_PROJECT_DIR}
  script:
    # Build aws-greengrass-parsec-provider (and dependencies) without tests
    # Tests use Docker-In-Docker which is not supported in Gitlab CI pipelines
    - |
      SRC_URL="https://github.com/awslabs/aws-greengrass-labs-parsec-provider.git"
      SRC_DIR="aws-greengrass-labs-parsec-provider"
      SRC_REF="535a87b1e499d348aaaa27ad6085249499b75e4d"
      BUILD_TS="1"
      OUTPUT_DIR="parsec-greengrass-plugin/target"
      PATCH_DIR="$CI_PROJECT_DIR/.gitlab/patches"
      PACKAGE_PATH="${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic"
      PACKAGE_NAME="greengrass-parsec-plugin"
      PLUGIN_VER="1.0.0"
      PLUGIN_NAME="aws.greengrass.crypto.ParsecProvider.jar"
      git clone ${SRC_URL} &&
      cd ${SRC_DIR} &&
      git checkout ${SRC_REF} &&
      git am "${PATCH_DIR}"/aws-greengrass-parsec-provider/*.patch &&
      git submodule update --init &&
      cd parsec-client-java &&
      git am "${PATCH_DIR}/parsec-client-java/"*.patch &&
      git submodule update --init --recursive &&
      cd .. &&
      mvn -N io.takari:maven:0.7.7:wrapper &&
      ./mvnw clean source:jar install \
        -DskipTests=true -Dts=${BUILD_TS} \
        --projects 'org.parallaxsecond:parsec-protobuf-java' &&
      mvn -N io.takari:maven:0.7.7:wrapper &&
      ./mvnw clean source:jar install \
        -DskipTests=true -Dts=${BUILD_TS} \
        --projects 'org.parallaxsecond:parsec-interface-java' &&
      mvn -N io.takari:maven:0.7.7:wrapper &&
      ./mvnw clean source:jar install \
        -DskipTests=true -Dts=${BUILD_TS} \
        --projects 'org.parallaxsecond:parsec-client-java' &&
      mvn -N io.takari:maven:0.7.7:wrapper &&
      ./mvnw clean source:jar install \
        -DskipTests=true -Dts=${BUILD_TS} \
        --projects 'org.parallaxsecond:parsec-jca-java' &&
      ./mvnw initialize &&
      ./mvnw clean package \
        --define maven.test.skip=true \
        --projects 'org.parallaxsecond:parsec-greengrass-plugin' &&
      cp ${OUTPUT_DIR}/*.jar ${CI_PROJECT_DIR} &&
      set +x &&
      curl --fail-with-body --header "JOB-TOKEN: $CI_JOB_TOKEN" \
      --upload-file ${OUTPUT_DIR}/${PLUGIN_NAME} \
      "${PACKAGE_PATH}/${PACKAGE_NAME}/${PLUGIN_VER}/${PLUGIN_NAME}" || true
  when: manual
