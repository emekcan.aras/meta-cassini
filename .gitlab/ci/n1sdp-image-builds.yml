# SPDX-FileCopyrightText: Copyright (c) 2023-2024, Linaro Limited.
#
# SPDX-FileCopyrightText: <text>Copyright 2022-2023 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT
---
# Conditions
.if-n1sdp-greengrass: &if-n1sdp-greengrass
  if: '($BUILD_IMAGE =~ /all-images/ || $BUILD_IMAGE =~ /greengrass/) &&
      ($BUILD_PLATFORM =~ /all-platforms/ || $BUILD_PLATFORM =~ /n1sdp/)'

.if-n1sdp: &if-n1sdp
  if: '($BUILD_IMAGE =~ /all-images/) &&
      ($BUILD_PLATFORM =~ /all-platforms/ || $BUILD_PLATFORM =~ /n1sdp/)'

.generator-n1sdp:
  extends: .generator
  variables:
    MACHINE: n1sdp
    SUPPORTED_TESTS: 'none'

# The rules set on each job in this file are only intended to be used to select
# when an image is built and so should be configured to always build an image
# if it's selected on manual or scheduled jobs via the BUILD_* variables or
# if files in the repo that would effect the image are changed.
# The SUPPORTED_TESTS variable on each image it intended to list the test suites
# that can be run on an image with the provided lava test files.
#
# To change what tests are run, modifications should be made to the setting of
# the can_run,force and should_run jinja2 variables.

# N1SDP + k3s builds
cassini/n1sdp:
  extends: .generator-n1sdp
  variables:
    SUPPORTED_TESTS: 'acs,sanity'
    NUMBER_OF_SEQUENCE_FILES: 10
  rules:
    - if: '($BUILD_IMAGE =~ /all-images/ || $BUILD_IMAGE =~ /k3s/) &&
           ($BUILD_PLATFORM =~ /all-platforms/ || $BUILD_PLATFORM =~ /n1sdp/) &&
           ($FREQUENCY == "weekly")'
      variables:
        NUMBER_OF_SEQUENCE_FILES: 1
    - <<: *if-n1sdp
    - !reference [".build-image:rules:cassini", rules]
    - !reference [".build-image:rules:n1sdp", rules]

cassini/dev/n1sdp:
  extends: .generator-n1sdp
  rules:
    - <<: *if-n1sdp
    - !reference [".build-image:rules:cassini", rules]
    - !reference [".build-image:rules:dev", rules]
    - !reference [".build-image:rules:n1sdp", rules]

cassini/dev/tests/n1sdp:
  extends: .generator-n1sdp
  variables:
    SUPPORTED_TESTS: 'ptest'
  rules:
    - <<: *if-n1sdp
    - !reference [".build-image:rules:cassini", rules]
    - !reference [".build-image:rules:dev", rules]
    - !reference [".build-image:rules:tests", rules]
    - !reference [".build-image:rules:n1sdp", rules]

# N1SDP + greengrass builds
cassini/dev/greengrass/n1sdp:
  extends: .generator-n1sdp
  variables:
    SUPPORTED_TESTS: 'idt_greengrass'
  rules:
    - <<: *if-n1sdp-greengrass
    - !reference [".build-image:rules:cassini", rules]
    - !reference [".build-image:rules:dev", rules]
    - !reference [".build-image:rules:greengrass", rules]
    - !reference [".build-image:rules:n1sdp", rules]
