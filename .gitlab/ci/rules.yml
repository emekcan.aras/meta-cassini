# SPDX-FileCopyrightText: Copyright (c) 2023-2024, Linaro Limited.
#
# SPDX-FileCopyrightText: <text>Copyright 2022-2024 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT
---
# Conditions
.if-merge-request: &if-merge-request
  if: '$CI_MERGE_REQUEST_IID'

.if-merge-request-or-push: &if-merge-request-or-push
  if: '$CI_MERGE_REQUEST_IID || $CI_PIPELINE_SOURCE == "push"'

.if-scheduled-or-manual: &if-scheduled-or-manual
  if: '$CI_PIPELINE_SOURCE == "schedule" || $CI_PIPELINE_SOURCE == "web"'

.if-parent-scheduled-or-manual: &if-parent-scheduled-or-manual
  if: '$PARENT_PIPELINE_SOURCE == "schedule" ||
       $PARENT_PIPELINE_SOURCE == "web"'

.if-parent-pipeline: &if-parent-pipeline
  if: '$CI_PIPELINE_SOURCE == "parent_pipeline"'

# Changes patterns
.ci-patterns: &ci-patterns
  - ".gitlab-ci.yml"
  - ".gitlab/ci/cassini-build.yml"
  - ".gitlab/ci/collate-results.yml"
  - ".gitlab/ci/docs-build.yml"
  - ".gitlab/ci/lava-test.yml"
  - ".gitlab/ci/rules.yml"
  - ".gitlab/ci/trigger-image-builds.yml"
  - ".gitlab/ci/yocto-qa.yml"
  - ".gitlab/ci/templates/image_build.yml.j2"
  - ".gitlab/scripts/**/*"

.ci-docker-images-patterns: &ci-docker-images-patterns
  - ".gitlab/Dockerfiles/**/*"
  - ".gitlab/ci/docker-image-builds.yml"

.ci-n1sdp-patterns: &ci-n1sdp-patterns
  - ".gitlab/ci/n1sdp-image-builds.yml"
  - ".gitlab/lava/n1sdp/*"

.ci-corstone1000-patterns: &ci-corstone1000-patterns
  - ".gitlab/ci/corstone1000-fvp-image-builds.yml"
  - ".gitlab/ci/corstone1000-mps3-image-builds.yml"
  - ".gitlab/lava/corstone1000*/*"

.ci-kv260-patterns: &ci-kv260-patterns
  - ".gitlab/ci/kv260-image-builds.yml"
  - ".gitlab/lava/zynqmp-kria-starter-psa/*"

.kas-common: &kas-common
  - "kas/cassini.yml"
  - "kas/ci.yml"
  - "kas/include/cassini-base.yml"
  - "kas/include/cassini-release.yml"

.kas-dev: &kas-dev
  - "kas/dev.yml"

.kas-tests: &kas-tests
  - "kas/tests.yml"

.kas-greengrass: &kas-greengrass
  - "kas/greengrass.yml"

.kas-n1sdp: &kas-n1sdp
  - "kas/n1sdp.yml"
  - "kas/include/arm-base.yml"
  - "kas/include/arm-machines.yml"

.kas-corstone1000: &kas-corstone1000
  - "kas/corstone1000*.yml"
  - "kas/include/arm-base.yml"
  - "kas/include/arm-machines.yml"

.kas-kv260: &kas-kv260
  - "kas/zynqmp-kria-starter-psa.yml"
  - "kas/include/arm-base.yml"
  - "kas/include/xilinx-machines.yml"

.build-common: &build-common
  - "meta-cassini-distro/**/*"

.build-tests: &build-tests
  - "meta-cassini-tests/classes/**/*"
  - "meta-cassini-tests/conf/**/*"
  - "meta-cassini-tests/poky/**/*"
  - "meta-cassini-tests/recipes-core/**/*"
  - "meta-cassini-tests/recipes-tests/bats/**/*"
  - "meta-cassini-tests/recipes-tests/runtime-integration-tests/container*.*"
  - "meta-cassini-tests/recipes-tests/runtime-integration-tests/container*/**/*"
  - "meta-cassini-tests/recipes-tests/runtime-integration-tests/user*.*"
  - "meta-cassini-tests/recipes-tests/runtime-integration-tests/user*/**/*"
  - "meta-cassini-tests/recipes-tests/runtime-integration-tests/runtime*.*"
  - "meta-cassini-tests/recipes-tests/runtime-integration-tests/runtime*/**/*"
  - "meta-cassini-tests/recipes-tests/runtime-integration-tests/files/**/*"
  - "meta-cassini-tests/recipes-tests/runtime-integration-tests/optee*.*"
  - "meta-cassini-tests/recipes-tests/runtime-integration-tests/optee*/**/*"
  - "meta-cassini-tests/recipes-tests/runtime-integration-tests/psa*.*"
  - "meta-cassini-tests/recipes-tests/runtime-integration-tests/psa/**/*"
  - "meta-cassini-tests/recipes-tests/runtime-integration-tests/parsec*.*"
  - "meta-cassini-tests/recipes-tests/runtime-integration-tests/parsec*/**/*"
  - "meta-cassini-tests/recipes-tests/runtime-integration-tests/k3s*.*"
  - "meta-cassini-tests/recipes-tests/runtime-integration-tests/k3s*/**/*"

.build-corstone1000: &build-corstone1000
  - "meta-cassini-tests/recipes-devtools/**/*"

# Build rules
.build-image:rules:n1sdp:
  rules:
    - <<: *if-merge-request-or-push
      changes: *ci-n1sdp-patterns
    - <<: *if-merge-request-or-push
      changes: *kas-n1sdp

.build-image:rules:corstone1000:
  rules:
    - <<: *if-merge-request-or-push
      changes: *ci-corstone1000-patterns
    - <<: *if-merge-request-or-push
      changes: *kas-corstone1000
    - <<: *if-merge-request-or-push
      changes: *build-corstone1000

.build-image:rules:kv260:
  rules:
    - <<: *if-merge-request-or-push
      changes: *ci-kv260-patterns
    - <<: *if-merge-request-or-push
      changes: *kas-kv260

.build-image:rules:greengrass:
  rules:
    - <<: *if-merge-request-or-push
      changes: *kas-greengrass

.build-image:rules:cassini:
  rules:
    - <<: *if-merge-request-or-push
      changes: *ci-patterns
    - <<: *if-merge-request-or-push
      changes: *ci-docker-images-patterns
    - <<: *if-merge-request-or-push
      changes: *kas-common
    - <<: *if-merge-request-or-push
      changes: *build-common

.build-image:rules:dev:
  rules:
    - <<: *if-merge-request-or-push
      changes: *kas-dev

.build-image:rules:tests:
  rules:
    - <<: *if-merge-request-or-push
      changes: *kas-tests
    - <<: *if-merge-request-or-push
      changes: *build-tests

.build-image:rules:scheduled-or-manual:
  rules:
    - <<: *if-scheduled-or-manual

.layer-check:rules:
  rules:
    - <<: *if-parent-pipeline
      changes: *ci-patterns
    - <<: *if-merge-request
    - <<: *if-parent-scheduled-or-manual
