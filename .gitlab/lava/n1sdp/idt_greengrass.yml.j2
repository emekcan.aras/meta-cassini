# SPDX-FileCopyrightText: Copyright (c) 2024, Linaro Limited.
#
# SPDX-License-Identifier: MIT
---

device_type: n1sdp
job_name: {{ CI_JOB_NAME }}
timeouts:
  job:
    minutes: 180

{%- if FREQUENCY == 'adhoc' %}
priority: medium
{%- else %}
priority: low
{%- endif %}
visibility:
  group:
    - cassini

metadata:
  source: {{ CI_PROJECT_URL }}
  path: .gitlab/lava/n1sdp/idt_greengrass.yml.j2
  gitlab-job-url: {{ CI_JOB_URL }}

notify:
  criteria:
    status: finished
  callbacks:
    - url: "{{ CI_API_V4_URL }}/projects/{{ CI_PROJECT_ID }}/jobs/{{ TEST_COMPLETE_JOB_ID }}/play"
      method: POST
      token: "{{ LAVA_CALLBACK_TOKEN }}"
      header: "PRIVATE-TOKEN"
      dataset: minimal

context:
  extra_nfsroot_args: ',vers=3'
  extra_kernel_args: rootwait

actions:

#
# Deploy a firmware bundle with the customized "uefi.bin" installed. This
# enables an EFI network driver, allowing us to TFTP boot from GRUB.
#
- deploy:
    namespace: recovery
    timeout:
      minutes: 5
    failure_retry: 2
    to: flasher
    images:
      recovery_image:
        url: {{ LAVA_STATIC_FILES_SERVER_URL }}/n1sdp/4/n1sdp-board-firmware-force-netboot.zip
        compression: zip

- deploy:
    namespace: debian
    timeout:
      minutes: 10
    failure_retry: 2
    to: tftp
    os: debian
    kernel:
      url: {{ LAVA_STATIC_FILES_SERVER_URL }}/n1sdp/4/debian/linux
      type: image
    ramdisk:
      url: {{ LAVA_STATIC_FILES_SERVER_URL }}/n1sdp/4/debian/ramdisk.img
    nfsrootfs:
      url: {{ LAVA_STATIC_FILES_SERVER_URL }}/n1sdp/4/debian/debian-buster-arm64-rootfs.tar.xz
      compression: xz

- boot:
    namespace: recovery
    timeout:
      minutes: 5
    method: minimal
    parameters:
      kernel-start-message: ''
    prompts: ['Cmd>']

- boot:
    namespace: uart1
    method: new_connection
    connection: uart1

- boot:
    namespace: uart3
    method: new_connection
    connection: uart3

- boot:
    namespace: debian
    connection-namespace: uart1
    timeout:
      minutes: 5
    method: grub
    commands: nfs
    prompts:
      - '/ # '

- test:
    namespace: debian
    timeout:
      minutes: 5
    definitions:
      - repository:
          metadata:
            format: Lava-Test Test Definition 1.0
            name: install-dependencies
            description: '"Install dependencies for secondary media deployment"'
            os:
              - debian
            scope:
              - functional
          run:
            steps:
              - apt-get update -q
              - apt-get install -qy bmap-tools
              - find /dev/disk
        from: inline
        name: install-dependencies
        path: inline/install-dependencies.yaml

#
# Deploy the primary board firmware bundle (this time without the additional
# network driver).
#
- deploy:
    namespace: recovery
    timeout:
      minutes: 5
    failure_retry: 2
    to: flasher
    timeout:
      minutes: 2
    images:
      recovery_image:
        url: "{{ FIRMWARE_ARTIFACT }}"
        compression: zip

- deploy:
    namespace: secondary_media
    connection-namespace: uart1
    timeout:
      minutes: 15
    to: usb
    os: oe
    images:
      image:
        url: "{{ IMAGE_ARTIFACT }}"
        compression: gz
      bmap:
        url: "{{ BMAP_ARTIFACT }}"
    uniquify: false
    device: usb_storage_device
    writer:
      tool: /usr/bin/bmaptool
      options: copy {DOWNLOAD_URL} {DEVICE}
      prompt: 'bmaptool: info'
    tool:
      prompts: ['copying time: [0-9ms\.\ ]+, copying speed [0-9\.]+ MiB\/sec']

#
# Do not verify the flash second time around as cached serial output on the
# connection will immediately match the prompt.
#

- boot:
    namespace: secondary_media
    timeout:
      minutes: 10
    method: minimal
    auto_login:
      login_prompt: 'n1sdp login:.*'
      username: cassini
      login_commands:
      - cassini123
      - cassini123
      - sudo su -
    prompts:
      - 'New password: '
      - 'Re-enter new password: '
      - '@n1sdp:.+\$ '
      - '@n1sdp:.+# '
    transfer_overlay:
      download_command: wget -S
      unpack_command: tar -C / -xzf

# N1SDP
- test:
    namespace: secondary_media
    timeout:
      minutes: 3
    definitions:
      - repository:
          metadata:
            format: Lava-Test Test Definition 1.0
            name: Greengrass-Sanity-Test
            description: 'Greengrass-Sanity-Test'
            os:
              - oe
            scope:
              - functional
          run:
            steps:
              - export GG_HOME={{ GG_HOME }}
              - export IOT_KEY_NAME=rsa-signed-key-{{ MACHINE }}-{{ CI_JOB_ID }}
              - sudo mkdir /tmp/idt
              - sudo chown -R cassini:cassini /tmp/idt
              - sudo parsec-tool create-rsa-key -s --key-name $IOT_KEY_NAME
              - sudo parsec-tool create-csr --key-name $IOT_KEY_NAME --cn $IOT_KEY_NAME >$GG_HOME/$IOT_KEY_NAME.csr
              - sudo chmod 755 $GG_HOME
              - sudo chown -R cassini:cassini $GG_HOME
        from: inline
        name: Greengrass-Sanity-Test
        path: inline/Greengrass-Sanity-Test.yaml

# Tester - Provision Device Under Test
- test:
    namespace: secondary_media
    docker:
      image: {{ IDT_GREENGRASS_IMAGE }}
    timeout:
      minutes: 3
    definitions:
      - repository:
          metadata:
            format: Lava-Test Test Definition 1.0
            name: Greengrass-Provision-DUT
            description: "Greengrass Provision DUT"
            os:
            - oe
            scope:
            - functional
          run:
            steps:
              - aws configure set aws_access_key_id {{ AWS_ACCESS_KEY_ID }}
              - aws configure set aws_secret_access_key {{ AWS_SECRET_ACCESS_KEY }}
              - aws configure set region {{ AWS_DEFAULT_REGION }}
              - export IOT_THING_NAME=Device-Cassini-Greengrass-Thing-{{ MACHINE }}-{{ CI_JOB_ID }}
              - export THING_IP=$(lava-target-ip)
              - export IOT_KEY_NAME=rsa-signed-key-{{ MACHINE }}-{{ CI_JOB_ID }}
              - export IOT_TE_ROLE_ALIAS={{ IOT_TE_ROLE_ALIAS }}
              - export IOT_TE_ROLE_POLICY_ALIAS={{ IOT_TE_ROLE_POLICY_ALIAS }}
              - export IOT_THING_POLICY={{ IOT_THING_POLICY }}
              - export IOT_THING_GROUP={{ IOT_THING_GROUP }}
              - mkdir -p ${GG_HOME}
              - scp -o "StrictHostKeyChecking no" cassini@$THING_IP:$GG_HOME/$IOT_KEY_NAME.csr $GG_HOME/
              - /tester/scripts/iot_device_provision.sh
              - rm $GG_HOME/$IOT_KEY_NAME.csr
              - scp -o "StrictHostKeyChecking no" $GG_HOME/* cassini@$THING_IP:$GG_HOME/
        from: inline
        name: Greengrass-Provision-DUT
        path: inline/Greengrass-Provision-DUT.yaml

# N1SDP - Start Greengrass with generated parameters
- test:
    namespace: secondary_media
    timeout:
      minutes: 10
    definitions:
      - repository:
          metadata:
            format: Lava-Test Test Definition 1.0
            name: Greengrass-Setup-DUT
            description: 'Greengrass Setup DUT'
            os:
              - oe
            scope:
              - functional
          run:
            steps:
              - export IOT_THING_NAME=Device-Cassini-Greengrass-Thing-{{ MACHINE }}-{{ CI_JOB_ID }}
              - export IOT_THING_GROUP={{ IOT_THING_GROUP }}
              - export AWS_DEFAULT_REGION={{ AWS_DEFAULT_REGION }}
              - export GG_HOME={{ GG_HOME }}
              - sudo systemctl stop greengrass
              - rm -rf ${GG_HOME}/logs/*
              - |
              - sudo -E java -Droot="${GG_HOME}" -Dlog.store=FILE -jar "${GG_HOME}/alts/init/distro/lib/Greengrass.jar"
                             --init-config "${GG_HOME}/generated_config.yml"
                             --trusted-plugin "${GG_HOME}/aws.greengrass.crypto.ParsecProvider.jar"
                             --aws-region "${AWS_DEFAULT_REGION}"
                             --thing-name "${IOT_THING_NAME}"
                             --thing-group-name "${IOT_THING_GROUP}"
                             --component-default-user ggc_user:ggc_group
                             --setup-system-service true
                             --start true
                             --provision false
              - |
                until
                  systemctl is-active --quiet greengrass
                do
                  sleep 15
                done
        from: inline
        name: Greengrass-Setup-DUT
        path: inline/Greengrass-Setup-DUT.yaml

# Tester - Run IoT device tester
- test:
    namespace: secondary_media
    docker:
      image: {{ IDT_GREENGRASS_IMAGE }}
    timeout:
      minutes: 120
    definitions:
      - repository:
          metadata:
            format: Lava-Test Test Definition 1.0
            name: Greengrass-Run-IDT
            description: "Greengrass Run IDT"
            os:
            - oe
            scope:
            - functional
          run:
            steps:
              - aws configure set aws_access_key_id {{ AWS_ACCESS_KEY_ID }}
              - aws configure set aws_secret_access_key {{ AWS_SECRET_ACCESS_KEY }}
              - aws configure set region {{ AWS_DEFAULT_REGION }}
              - export THING_IP=$(lava-target-ip)
              - export TARGET_MACHINE={{ MACHINE }}
              - export IDT_ROLE={{ IDT_ROLE }}
              - export TEST_TIMEOUT_MULTIPLIER=2
              - export TEST_SESSION_NAME=IDT-Session-{{ CI_JOB_ID }}
              - export IDT_ROLE_SESSION_DURATION={{ IDT_ROLE_SESSION_DURATION }}
              - /tester/scripts/generate_device_config.sh
              - lava-test-case cassini-greengrass-tests --shell "timeout --foreground 110m /tester/scripts/run_tests.sh"
              - export gg_report_path=$(find /tester/devicetester_greengrass_v2_linux/results/ -xdev -name GGV2Q_Report.xml)
              - jfrog config add --insecure-tls=true artifactory-aws --interactive=false --artifactory-url={{ ARTIFACTORY_AWS_URL }} --user={{ ARTIFACTORY_USER }} --password={{ ARTIFACTORY_PASS }};
              - |
                cat << EOF > ./upload_spec.json
                {
                  "files": [
                    {
                      "pattern": "${gg_report_path}",
                      "target": "{{ ARTIFACTORY_NAMESPACE }}.{{ ARTIFACTORY_TMP_REPO }}/{{ CI_PROJECT_NAME }}/{{ CI_JOB_ID }}/greengrass-results/TEST-Greengrass-test-report.xml",
                      "flat": "true",
                      "recursive": "true"
                    }
                  ]
                }
                EOF
              - build_name="{{ ARTIFACTORY_NAMESPACE }}/{{ CI_PROJECT_PATH }}/{{ CI_JOB_NAME }}"
              - jfrog rt upload --insecure-tls=true --spec=upload_spec.json --build-name="${build_name}" --build-number="{{ CI_JOB_ID }}"
              # Collect environment variables and attach them to a build.
              - jfrog rt build-collect-env "${build_name}" "{{ CI_JOB_ID }}"
              # Publish build info to Artifactory and discard old builds
              - |
                jfrog rt build-publish --insecure-tls=true "${build_name}" "{{ CI_JOB_ID }}" --build-url="{{ CI_PROJECT_URL }}" &&
                jfrog rt build-discard --insecure-tls=true "${build_name}" --max-builds=10 --delete-artifacts --async ||
                echo "Info: build info not supported by Artifactory"
        from: inline
        name: Greengrass-Run-IDT
        path: inline/Greengrass-Run-IDT.yaml

# Tester - Cleanup
- test:
    namespace: secondary_media
    docker:
      image: {{ IDT_GREENGRASS_IMAGE }}
    timeout:
      minutes: 5
    definitions:
      - repository:
          metadata:
            format: Lava-Test Test Definition 1.0
            name: Greengrass-Cleanup-Tests
            description: "Greengrass Cleanup Tests"
            os:
            - oe
            scope:
            - functional
          run:
            steps:
              - aws configure set aws_access_key_id {{ AWS_ACCESS_KEY_ID }}
              - aws configure set aws_secret_access_key {{ AWS_SECRET_ACCESS_KEY }}
              - aws configure set region {{ AWS_DEFAULT_REGION }}
              - export IOT_THING_NAME=Device-Cassini-Greengrass-Thing-{{ MACHINE }}-{{ CI_JOB_ID }}
              - /tester/scripts/aws_cleanup.sh
        from: inline
        name: Greengrass-Cleanup-Tests
        path: inline/Greengrass-Cleanup-Tests.yaml
