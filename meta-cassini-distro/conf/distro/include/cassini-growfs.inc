# SPDX-FileCopyrightText: Copyright (c) 2024, Linaro Limited.
#
# SPDX-License-Identifier: MIT

CASSINI_ROOTFS_EXTRA_SPACE = "0"

PACKAGECONFIG:append:pn-systemd = " repart openssl"

IMAGE_INSTALL:append:cassini = " cassini-growfs"
