# SPDX-FileCopyrightText: Copyright (c) 2024, Linaro Limited.
#
# SPDX-FileCopyrightText: <text>Copyright 2024 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT

VIRTUAL-RUNTIME_cloud_service ??= "k3s-cloud"
IMAGE_FEATURES:append:cassini = " cloud-service"

include ${@bb.utils.contains(\
'VIRTUAL-RUNTIME_cloud_service','k3s-cloud', \
'cloud-services/k3s.inc', '', d)}

include ${@bb.utils.contains(\
'VIRTUAL-RUNTIME_cloud_service','greengrass-cloud', \
'cloud-services/greengrass.inc', '', d)}
