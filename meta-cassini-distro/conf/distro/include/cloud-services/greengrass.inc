# SPDX-FileCopyrightText: Copyright (c) 2024, Linaro Limited.
#
# SPDX-License-Identifier: MIT

CASSINI_ROOTFS_EXTRA_SPACE ?= "2000000"

IMAGE_INSTALL:append = " greengrass-parsec-plugin"

