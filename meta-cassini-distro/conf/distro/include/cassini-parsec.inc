# SPDX-FileCopyrightText: Copyright (c) 2024, Linaro Limited.
#
# SPDX-FileCopyrightText: <text>Copyright 2022-2023 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT

# Config specific to the cassini-parsec distro feature, enabled using
# DISTRO_FEATURES
VIRTUAL-RUNTIME_security_provider ??= "psa-provider"

IMAGE_INSTALL:append = " parsec-service \
                         parsec-tool"

DISTROOVERRIDES .= ":cassini-parsec"
