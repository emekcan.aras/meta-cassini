# SPDX-FileCopyrightText: Copyright (c) 2024, Linaro Limited.
#
# SPDX-License-Identifier: MIT

SUMMARY = "Do not send host name when requesting IP address from DHCP"
DESCRIPTION = "Make sure we do not send host name to DHCP server for any \
    request of an IP address, this avoids server assigning a different IP \
    address to the same MAC address after a reboot if the host name was \
    not known during the first boot"
HOMEPAGE = "https://cassini.readthedocs.io/en/latest/"
LICENSE = "MIT"

# nooelint: oelint.var.licenseremotefile
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI = "file://20-wired.network"

S = "${WORKDIR}/home"

inherit allarch features_check

FILES:${PN} += "${sysconfdir}/systemd/network/20-wired.network"

REQUIRED_DISTRO_FEATURES = "systemd"

do_install() {
    install -d "${D}${sysconfdir}/systemd/network"
    install --mode="644" "${WORKDIR}/20-wired.network" "${D}${sysconfdir}/systemd/network/20-wired.network"
}
