# SPDX-FileCopyrightText: <text>Copyright 2023 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT

# Allow the inclusion of libts in all builds so parsec can use it

COMPATIBLE_MACHINE = "aarch64"
