# nooelint: oelint.var.mandatoryvar
#
# SPDX-FileCopyrightText: <text>Copyright 2022-2023 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT

SUMMARY = "CASSINI base image core config with common packages"

IMAGE_LINGUAS = ""

LICENSE = "MIT"

inherit cassini-image
