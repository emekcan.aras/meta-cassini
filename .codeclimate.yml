# SPDX-FileCopyrightText: Copyright (c) 2023-2024, Linaro Limited.
#
# SPDX-FileCopyrightText: <text>Copyright 2022-2024 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT
---
version: "2"
plugins:
  structure:
    exclude_patterns:
      - "documentation/variables.py"
  pep8:
    enabled: true
  sonar-python:
    enabled: true
  shellcheck:
    enabled: true
    file_types:
      - "shell script"
      - "bats script"
      - "bash script"
  fixme:
    enabled: true
    exclude_patterns:
      - "/layers/"
      - "/Dangerfile"
  cspell:
    enabled: true
    exclude_patterns:
      - "CHANGELOG.md"
      - ".gitignore"
      - "**.cfg"
      - "documentation/Makefile"
      - "documentation/requirements.*"
      - "**.patch"
    dict_path: ".dictionary"
  yamllint:
    enabled: true
  oelint-adv:
    enabled: true
    suppressions:
      # Not all suggested variables are applicable to all files.
      - 'oelint.var.suggestedvar'
      # 'BBCLASSEXTEND' variable will only be set when required.
      # Hence, we don't need to set it for all recipes.
      - 'oelint.var.bbclassextend'
      # Some filenames have - in the version number.
      - 'oelint.file.underscores'
      # Code Climate disables network access which prevents
      # oelint-adv from pinging the URLs.
      - 'oelint.vars.homepageping'
      # In some cases, we want to use the include directive.
      # But for some reason, the in-line suppression for this
      # check doesn't work.
      - 'oelint.file.requireinclude'
      # The oelint-adv is not able to find .inc files from other layers
      # and the in-line suppression for this check doesn't work.
      - 'oelint.file.requirenotfound'
      # This check is done for all variables,
      # and suppressing it for each case is not feasible.
      - 'oelint.vars.multilineident'

exclude_patterns:
  - ".csslintrc"
  - ".eslintrc.yml"
  - ".eslintignore"
  - ".rubocop.yml"
  - "coffeelint.json"
  - "/layers/"
  - "/build/"
  - "*.png"
  - "*.pyc"
  - ".config.yaml*"
