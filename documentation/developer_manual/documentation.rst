..
 # SPDX-FileCopyrightText: Copyright (c) 2024, Linaro Limited.
 #
 # SPDX-FileCopyrightText: <text>Copyright 2022-2023 Arm Limited and/or its
 # affiliates <open-source-office@arm.com></text> 
 #
 # SPDX-License-Identifier: MIT

##########################
Building the documentation
##########################

The Cassini project is currently configured so that `Read the Docs` builds the
project documentation using Ubuntu 22.04 and Python 3.10.

The sources for the documentation are found in the ``documentation`` folder.
To setup the host to build the documentation locally, install the required
packages on the host as follows:

.. code-block:: shell

    python3 -m pip install -U -r documentation/requirements.txt

To build and generate the documentation in html format, run:

.. code-block:: shell

    sphinx-build -b html -a -W documentation public

To render and explore the documentation, simply open
`meta-cassini/public/index.html` in a web browser.
