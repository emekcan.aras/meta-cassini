..
 # SPDX-FileCopyrightText: Copyright (c) 2024, Linaro Limited.
 #
 # SPDX-License-Identifier: MIT

#################################################
Amazon Web Services (AWS) IoT Device Tester (IDT)
#################################################

AWS IoT Device Tester (IDT) is a downloadable testing framework that helps us
validate IoT devices, see |AWS IoT Device Tester for Greengrass V2|_.

The IDT is installed with tools and configured in a Docker container image
with required credentials of AWS account for testing Greengrass on a device.

These credentials are required by ``aws-cli`` to perform necessary setup for
Device Under Test (DUT) and needs to be configured as GitLab variables to be
used by GitLab pipelines:

* `AWS_ACCESS_KEY_ID`
* `AWS_SECRET_ACCESS_KEY`
* `AWS_DEFAULT_REGION`

The Docker container provisions the Device to work with AWS and IDT and
generates a config file, which is used on the DUT to restart the Greengrass
service. Then IDT runs the configured required tests and uploads to
artifactory (optional).

|

.. image:: ../images/idt_greengrass.svg
  :align: center

|

********
Overview
********

A brief overview of IDT setup and running it on Cassini GitLab CI/CD.

The IoT Device Tester is setup as follows:

.. _idt_setup_label:

* **IDT Setup** :

  The ``idt_setup.sh`` should be executed manually **once** to setup the
  **project** to use IoT Device Tester with AWS. It creates the required AWS
  Identity and Access Management (IAM) role, Internet of Things (IoT) Token
  exchange role, IoT role alias, IoT Thing Group and policies for IDT.
  The following parameters needs to be configured:

    * `GG_HOME` : Home directory of Greengrass service
    * `IDT_ROLE` : IAM role
    * `IDT_ROLE_POLICY` : IAM role policy
    * `IDT_ROLE_SESSION_DURATION` : IAM role session timeout duration
    * `IOT_TE_ROLE` : IoT Token exchange role
    * `IOT_TE_ROLE_POLICY` : IoT Token exchange policy
    * `IOT_TE_ROLE_ALIAS` : IoT Token exchange role alias
    * `IOT_TE_ROLE_POLICY_ALIAS` : IoT Token exchange policy alias
    * `IOT_THING_POLICY` : IoT thing policy
    * `IOT_THING_GROUP` : IoT thing group
    * `AWS_BOUNDARY_POLICY` : Boundary policy for IAM or IoT role (optional).

.. _provision_dut_label:

The IoT Device Tester is run in LAVA with the following steps:

* **Provision DUT**:

  This step is executed on IDT Docker container image for
  every DUT with unique ``IOT_THING_NAME``. The parsec tool is used to
  generate the signed RSA key and a certificate to work with AWS for IDT
  on DUT. Further, these parameters needs to be configured as GitLab
  variables to be used by GitLab pipelines
  when :ref:`setup <idt_setup_label>` was performed:

    * `GG_HOME`
    * `IOT_THING_GROUP`
    * `IOT_THING_POLICY`
    * `IOT_TE_ROLE_POLICY_ALIAS`
    * `IOT_TE_ROLE_ALIAS`

  The generated configuration is then transferred to DUT and the Greengrass
  service is restarted with folder permissions set.

.. _configure_idt_dut_label:

* **Configure IDT for DUT**:

  This step is used to configure IDT installed on Docker container image
  with details of DUT and require the following:

    * `THING_IP` : IP address of DUT
    * `TARGET_MACHINE` : Machine name of DUT
    * `TARGET_PORT` : Port number to be used (default 22)

* **Assume role and run IDT**:

  After performing, :ref:`provisioning <provision_dut_label>` and
  :ref:`configuring IDT <provision_dut_label>`.
  The following parameters are required to run the tests:

    * `TEST_SESSION_NAME` : Test session name (optional, :ref:`see <optional_assume_role_label>`)
    * `TEST_TIMEOUT_MULTIPLIER` : Set to extend the default timeout for tests

.. _optional_assume_role_label:

  This step will attempt to assume ``IDT_ROLE`` before running the IDT test
  suite. If this fails, the test suite will run with the permissions granted
  to the AWS user. These parameters needs to be configured as GitLab variables
  to be used by GitLab pipelines:

    * `IDT_ROLE`
    * `IDT_ROLE_SESSION_DURATION`

* **Cleanup**:

  This is used to perform cleanup of ``IOT_THING_NAME`` which
  represents the DUT name effectively when IDT tests have completed on CI/CD.
