..
 # SPDX-FileCopyrightText: Copyright (c) 2023, Linaro Limited.
 #
 # SPDX-License-Identifier: MIT

############
Code Quality
############

The CI/CD pipeline uses GitLab's Code Quality feature to perform static
analysis of code, scripts, and documentation.

*****
Usage
*****

The source and binaries of individual plugins are available to be used
by the project, some are provided from the code climate repository.

*******
Plugins
*******

* **structure**:

  Checks the structure of the code.

* **duplication**:

  Checks for duplication in the code.

* **cspell**:

  Runs the cspell spelling checker over files in a project.

* **inclusivity**:

  Checks for any potential non-inclusive terminology
  used in the project. As non-inclusive terminology is highly nuanced
  and context-dependent, the check simply highlights potential
  terminology to the user so that consideration can be made if an
  alternative language would be more appropriate.

* **oelint-adv**:

  This plugin runs an opinionated linter (see |oelint-adv|_) over bitbake
  recipes and checks them for conformance to the |OpenEmbedded style guide|_.

* **shell-check**:

  Gives warnings and suggestions for bash/sh shell scripts.

* **pep8**:

  Pep8 provides feedback on Python code style following the rules
  outlined in the PEP 8 style guide.

* **sonarpython**:

  Sonar static code analysis helps you build secure, maintainable,
  and high-quality Python software. Covering popular build systems,
  standards, and versions, ensuring security vulnerabilities are
  addressed at early stages of development.

* **fixme**:

  The fixme engine performs a case-sensitive search on the
  repository (see |fixme|_).

* **yamllint**:

  Validates the structure and syntax of yaml/yml files.
