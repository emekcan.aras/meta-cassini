# SPDX-FileCopyrightText: Copyright (c) 2023-2024, Linaro Limited.
#
# SPDX-FileCopyrightText: <text>Copyright 2022-2023 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT

# This file centralizes the variables and links used throughout the Cassini
# documentation. The dictionaries are converted to a single string that is used
# as the rst_prolog (see the Sphinx Configuration documentation at
# https://www.sphinx-doc.org/en/master/usage/configuration.html for more info).

# There are two types of key-value substitutions:
#     1. simple string replacements
#     2. replacement with a rendered hyperlink, where the key defines what the
#        rendered hyperlink text will be

# Prepend the key with "link:" to identify it as a Sphinx target name for use
# as a hyperlink. The "link:" prefix is dropped from the substitution name.
#
# For example:
#   "link:This URL": "www.arm.com"
#   "company name": "arm"
# Can be used as:
#   The |company name| website can be found at |This URL|_.
#
# Note the "_" which renders the substitution as a hyperlink is only possible
# because the variable is defined as a link, to be resolved as a Sphinx target.

import os

yocto_release = "scarthgap"
yocto_doc_version = "scarthgap"
corstone1000_doc_version = "scarthgap"
yocto_linux_version = "6.6"
kas_version = "4.3.2"
cassini_version = "v2.0.0"
cassini_branch = os.environ.get("READTHEDOCS_GIT_IDENTIFIER")

# Disable for spelling and forbidden words in links
# spell-checker:disable

general_links = {
    "link:Yocto Project Documentation":
        f"https://docs.yoctoproject.org/{yocto_doc_version}",
    "link:Yocto Release Process":
        "https://docs.yoctoproject.org/ref-manual/release-process.html",
    "link:kernel module compilation":
        f"https://docs.yoctoproject.org/{yocto_doc_version}/kernel-dev/"
        "common.html#building-out-of-tree-modules-on-the-target",
    "link:profiling and tracing":
        f"https://docs.yoctoproject.org/{yocto_doc_version}/profile-manual/"
        "index.html",
    "link:runtime package management":
        f"https://docs.yoctoproject.org/{yocto_doc_version}/dev-manual/"
        "common-tasks.html#using-runtime-package-management",
    "link:Multiple Configuration Build":
        f"https://docs.yoctoproject.org/{yocto_doc_version}/dev-manual/"
        "common-tasks.html#"
        "building-images-for-multiple-targets-using-multiple-configurations",
    "link:list of essential packages":
        f"https://docs.yoctoproject.org/{yocto_doc_version}/singleindex.html#"
        "required-packages-for-the-build-host",
    "link:Yocto Check Layer Script":
        f"https://docs.yoctoproject.org/{yocto_doc_version}/singleindex.html#"
        "yocto-check-layer-script",
    "link:Yocto buildtools":
        f"https://docs.yoctoproject.org/{yocto_doc_version}/singleindex.html#"
        "term-buildtools",
    "link:DEFAULTTUNE":
        f"https://docs.yoctoproject.org/{yocto_doc_version}/ref-manual/"
        "variables.html#term-DEFAULTTUNE",
    "link:Yocto Docker config":
        "https://git.yoctoproject.org/yocto-kernel-cache/tree/features/docker/"
        f"docker.cfg?h=yocto-{yocto_linux_version}",
    "link:Yocto K3s config":
        "https://git.yoctoproject.org/cgit/cgit.cgi/meta-virtualization/tree/"
        f"recipes-kernel/linux/linux-yocto/kubernetes.cfg?h={yocto_release}",
    "link:Yocto Xen config":
        "https://git.yoctoproject.org/cgit/cgit.cgi/yocto-kernel-cache/tree/"
        f"features/xen/xen.cfg?h=yocto-{yocto_linux_version}",
    "link:kas build tool":
        f"https://kas.readthedocs.io/en/{kas_version}/userguide.html",
    "link:kas Dependencies & installation":
        f"https://kas.readthedocs.io/en/{kas_version}/userguide.html#"
        "dependencies-installation",
    "link:meta-arm-bsp":
        "https://git.yoctoproject.org/meta-arm/tree/meta-arm-bsp/"
        f"documentation/n1sdp.md?h={yocto_release}",
    "link:meta-trustedsubstrate":
        "https://gitlab.com/Linaro/trustedsubstrate/meta-ts/-/"
        "blob/master/README.md",
    "link:TRS manifest":
        "https://gitlab.com/Linaro/trusted-reference-stack/trs-manifest/",
    "link:N1SDP Technical Reference Manual":
        "https://developer.arm.com/documentation/101489/0000",
    "link:Kria KV260 User Guide":
        "https://docs.xilinx.com/r/en-US/ug1089-kv260-starter-kit",
    "link:Kria KV260 Data sheet":
        "https://docs.xilinx.com/r/en-US/ds986-kv260-starter-kit",
    "link:Arm Corstone-1000 Technical Overview":
        "https://developer.arm.com/documentation/102360/0000",
    "link:Arm Corstone-1000 Software":
        f"https://corstone1000.docs.arm.com/en/{corstone1000_doc_version}/",
    "link:Test Report for CORSTONE1000-2023.06":
        "https://gitlab.arm.com/arm-reference-solutions/"
        "arm-reference-solutions-test-report/-/tree/master/embedded-a/"
        "corstone1000/CORSTONE1000-2023.06",
    "link:Clean Secure Flash Before Testing":
        f"https://corstone1000.docs.arm.com/en/{corstone1000_doc_version}/"
        "user-guide.html"
        "#clean-secure-flash-before-testing-applicable-to-fpga-only",
    "link:Test Report for CORSTONE1000-2023.11":
        "https://gitlab.arm.com/arm-reference-solutions/"
        "arm-reference-solutions-test-report/-/tree/master/embedded-a/"
        "corstone1000/CORSTONE1000-2023.11",
    "link:Test Report for CORSTONE1000-2024.06":
        "https://gitlab.arm.com/arm-reference-solutions/"
        "arm-reference-solutions-test-report/-/tree/master/embedded-a/"
        "corstone1000/CORSTONE1000-2024.06",
    "link:Fast Models Fixed Virtual Platforms (FVP) Reference Guide":
        "https://developer.arm.com/documentation/100966/1119",
    "link:Fast Models Reference Guide":
        "https://developer.arm.com/documentation/100964/1119/"
        "Introduction-to-Fast-Models/User-mode-networking",
    "link:PEP 8": "https://peps.python.org/pep-0008/",
    "link:pycodestyle Documentation":
        "https://pycodestyle.pycqa.org/en/latest/",
    "link:Shellcheck": "https://github.com/koalaman/shellcheck",
    "link:Shellcheck wiki pages":
        "https://github.com/koalaman/shellcheck/wiki/Checks",
    "link:Docker": "https://docs.docker.com",
    "link:K3s orchestration": "https://docs.k3s.io/",
    "link:yamllint documentation":
        "https://yamllint.readthedocs.io/en/stable/",
    "link:Yocto Package Test": "https://wiki.yoctoproject.org/wiki/Ptest",
    "link:Bash Automated Test System":
        "https://github.com/bats-core/bats-core",
    "link:Python Datetime Format Codes":
        "https://docs.python.org/3/library/datetime.html#"
        "strftime-and-strptime-format-codes",
    "link:Nginx": "https://www.nginx.com/",
    "link:Potential firmware damage notice":
        "https://community.arm.com/developer/tools-software/oss-platforms/"
        "w/docs/604/notice-potential-damage-to-n1sdp-boards-"
        "if-using-latest-firmware-release",
    "link:GitLab Issues": "https://gitlab.com/Linaro/cassini/meta-cassini/"
        "-/issues",
    "link:EULA": "https://developer.arm.com/downloads/"
        "-/arm-ecosystem-fvps/eula",
    "link:oelint-adv":
        "https://github.com/priv-kweihmann/oelint-adv",
    "link:OpenEmbedded style guide":
        "https://docs.yoctoproject.org/dev/contributor-guide/"
        "recipe-style-guide.html",
    "link:GitLab Templates":
        "https://gitlab.com/Linaro/cassini/gitlab-templates",
    "link:SCT parser":
        "https://gitlab.arm.com/systemready/edk2-test-parser",
    "link:fixme":
        "https://github.com/codeclimate/codeclimate-fixme",
    "link:Gitlab pipelines":
        "https://docs.gitlab.com/ee/ci/pipelines/",
    "link:LAVA":
        "https://validation.linaro.org/",
    "link:AWS IoT Device Tester for Greengrass V2":
        "https://docs.aws.amazon.com/greengrass/v2/developerguide/"
        "device-tester-for-greengrass-ug.html",
    "link:debug tweaks":
        "https://docs.yoctoproject.org/dev/ref-manual/"
        "features.html#image-features",
    "link:AWS IoT Greengrass V2":
        "https://docs.aws.amazon.com/greengrass/",
}

layer_definitions = {
    "meta-cassini contributions branch": f"``{yocto_release}-dev``",
    "meta-cassini repository":
        "https://gitlab.com/Linaro/cassini/meta-cassini",
    "meta-cassini repository host": "https://gitlab.com/Linaro",
    "meta-cassini remote":
        "https://gitlab.com/Linaro/cassini/meta-cassini.git",
    "meta-cassini branch": f"{cassini_branch}",
    "meta-cassini-bsp branch": f"{yocto_release}",
    "poky branch": f"{yocto_release}",
    "meta-openembedded branch": f"{yocto_release}",
    "meta-virtualization branch": f"{yocto_release}",
    "meta-security branch": f"{yocto_release}",
    "meta-clang branch": f"{yocto_release}",
    "meta-arm branch": f"{yocto_release}",
    "meta-secure-core branch": f"{yocto_release}",
    "meta-ts branch":  f"{yocto_release}",
    "meta-ledge-secure branch": "main",
    "meta-xilinx branch": f"{yocto_release}",
    "meta-aws branch": f"{yocto_release}",
    "poky revision": "ca27724b44031fe11b631ee50eb1e20f7a60009d",
    "meta-virtualization revision": "37c06acf58f9020bccfc61954eeefe160642d5f3",
    "meta-security revision": "11ea91192d43d7c2b0b95a93aa63ca7e73e38034",
    "meta-clang revision": "0acff283249842eb1f617b20c2ed4ebf9f8e3557",
    "meta-openembedded revision": "78a14731cf0cf38a19ff8bd0e9255b319afaf3a7",
    "meta-arm revision": "58268ddccbe2780de28513273e15193ee949987b",
    "meta-secure-core revision": "f3f928d097917b8a131044fe718440eb7f7e381b",
    "meta-cassini-bsp revision": "710eb7beeb91b1812dd01f3fe1ef1152a7b82277",
    "meta-aws revision": "1b562ef097390b1fe87ab4920e3120b12e948568",
    "meta-ts revision": "11ab1b7b250e1261cac8eeaa1b8460b9fa8dcc4c",
    "meta-ledge-secure revision": "25d02c9baaaeec88fb4bc18910fb8190cb1cd6c8",
    "meta-xilinx revision": "62523c2bc42b6b0c5d795373e5110a5c07ed52d8",
    "layer dependency statement":
        "The layer revisions are related to the "
        f"Cassini ``{cassini_version}`` release.",
}

# spell-checker:enable

other_definitions = {
    "kas version": f"{kas_version}",
}

# Potentially old definitions required for documenting migrations, changelog
release_definitions = {
    "v0.1:migration version": "v0.1",
    "v0.1:kas version": other_definitions["kas version"],
}


def generate_link(key, link):

    definition = f".. _{key}: {link}"
    key_mapping = f".. |{key}| replace:: {key}"
    return f"{definition}\n{key_mapping}"


def generate_replacement(key, value):

    replacement = f".. |{key}| replace:: {value}"
    return f"{replacement}"


def generate_rst_prolog():

    rst_prolog = ""

    for variables_group in [general_links,
                            layer_definitions,
                            other_definitions,
                            release_definitions]:

        for key, value in variables_group.items():
            if key.startswith("link:"):
                rst_prolog += generate_link(key.split("link:")
                                            [1], value) + "\n"
            else:
                rst_prolog += generate_replacement(key, value) + "\n"

    return rst_prolog
